﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DatabaseContextExtension
    {
        public static DatabaseContext RecreateDb(this DatabaseContext context)
        {
            return context;
        }

        private static void FillData(DatabaseContext context)
        {

            foreach (var role in Data.FakeDataFactory.Roles)
            {
                context.Set<Role>().Add(role);
            }

            foreach (var customer in Data.FakeDataFactory.Customers)
            {
                context.Set<Customer>().Add(customer);
            }

            foreach (var employee in Data.FakeDataFactory.Employees)
            {
                context.Set<Employee>().Add(employee);
            }

            context.SaveChanges();
        }
    }
}

﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = 1,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = 3,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = 2,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = 3,
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = 3,
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = 4,
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = 5,
                Name = "Театр",
            },
            new Preference()
            {
                Id = 6,
                Name = "Семья",
            },
            new Preference()
            {
                Id = 7,
                Name = "Дети",
            }
        };
        public static IEnumerable<CustomerPreference> CustomerPreferences
            => new List<CustomerPreference>()
        {
            new CustomerPreference { CustomerId=8,PreferenceId=5},
            new CustomerPreference { CustomerId=8,PreferenceId=6}
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = 8;
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений
                    }
                };

                return customers;
            }
        }
    }
}
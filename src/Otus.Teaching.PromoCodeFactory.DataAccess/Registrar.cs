﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class Registrar
    {
        public static IServiceCollection InstallEfService(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<DatabaseContext>(b => b.UseLazyLoadingProxies().UseSqlite(connectionString));
            return services;
        }

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;
public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {

    }
    DbSet<Employee> Employees { get; set; }
    DbSet<Role> Roles { get; set; }
    DbSet<Customer> Customers { get; set; }
    DbSet<Preference> Preferences { get; set; }
    DbSet<PromoCode> PromoCodes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.PreferenceId, cp.CustomerId });

        modelBuilder.Entity<Customer>()
                .HasMany(u => u.Preferences)
                .WithMany(m => m.Customers)
                .UsingEntity<CustomerPreference>();

        modelBuilder.Entity<Preference>()
               .HasMany(u => u.Customers)
               .WithMany(m => m.Preferences)
               .UsingEntity<CustomerPreference>();

        modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(20);
        modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(20);
        modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(50);

        modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(50);
        modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(500);

        modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(20);
        modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(20);
        modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(50);

        modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(20);

        modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(50);
        modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(150);
        modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(50);

        modelBuilder.Entity<Role>().HasData(Data.FakeDataFactory.Roles);
        modelBuilder.Entity<Employee>().HasData(Data.FakeDataFactory.Employees);
        modelBuilder.Entity<Customer>().HasData(Data.FakeDataFactory.Customers);
        modelBuilder.Entity<Preference>().HasData(Data.FakeDataFactory.Preferences);
        modelBuilder.Entity<CustomerPreference>().HasData(Data.FakeDataFactory.CustomerPreferences);
    }
}


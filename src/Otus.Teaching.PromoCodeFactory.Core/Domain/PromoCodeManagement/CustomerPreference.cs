﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
public class CustomerPreference
{
    public int CustomerId { get; set; }
    public virtual Customer Customer { get; set; }
    public int PreferenceId { get; set; }
    public virtual Preference Preference { get; set; }
}
